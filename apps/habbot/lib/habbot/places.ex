defmodule Habbot.Places do

  @moduledoc """
  Repository for places
  """
  use GenServer, start: {__MODULE__, :start_link, []}

  require Logger
  alias Habbot.Place

  @topic "places"


  @doc """
  Start the server
  """
  def start_link() do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end


  @doc """
  Create a new place
  """
  def create(name, description \\ nil, config) do
    GenServer.call(__MODULE__, {:create, name, description, config})
  end

  @doc """
  Update an existing place
  """
  def update(uuid, description \\ nil, config) do
    GenServer.call(__MODULE__, {:update, uuid, description, config})
  end

  @doc """
  Destroy the place
  """
  def destroy(place) do
    GenServer.call(__MODULE__, {:destroy, place})
  end


  @doc """
  List the places
  """
  def list do
    GenServer.call(__MODULE__, :list)
  end


  @doc """
  Link a value to a place
  """
  def link(place, value) do
    GenServer.call(__MODULE__, {:link, place, value})
  end


  @doc """
  Unlink a value
  """
  def unlink(place, value) do
    GenServer.call(__MODULE__, {:unlink, place, value})
  end


  @doc """
  Set the place's links
  """
  def set_links(place, values) do
    GenServer.call(__MODULE__, {:set_links, place, values})
  end


  @doc """
  Watch for places events
  """
  def watch() do
    Habbot.PubSub.subscribe(@topic)
  end


  @doc """
  Unwatch places events
  """
  def unwatch() do
    Habbot.PubSub.unsubscribe(@topic)
  end



  # Init the server
  def init(nil) do
    # Trap exits for values processes
    Process.flag(:trap_exit, true)

    # Init table, create state
    places_db =
      Application.get_env(:habbot, :data_dir, "./")
      |> Path.join("places.db")
      |> String.to_charlist

    {:ok, tid} = :dets.open_file(places_db, ram_file: true, auto_save: 10)
    state = %{table: tid, by_uuids: %{}, by_pids: %{}}

    state = :dets.foldl(fn {uuid, config}, state ->
      _start_place(state, uuid, config)
    end, state, state.table)

    {:ok, state}
  end


  # Handle create call
  def handle_call({:create, name, description, config}, _from, state) do
    uuid = UUID.uuid5(:nil, name)
    place = %Place{uuid: uuid, name: name, description: description, values: config.values}

    if :dets.insert_new(state.table, {uuid, place}) do
      Logger.debug "Creating new place #{name}"
      new_state = _start_place(state, uuid, place)
      Habbot.PubSub.broadcast(@topic, {:habbot, :place_created, place})
      {:reply, {:ok, uuid}, new_state}
    else
      Logger.warn "Place #{uuid} already exists"
      {:reply, {:error, :already_exists}, state}
    end
  end

  # Handle update call
  def handle_call({:update, uuid, description, config}, _from, state) do
    case :dets.lookup(state.table, uuid) do
      [] ->
        Logger.warn "Place #{uuid} not found"
        {:reply, {:error, :place_not_found}, state}
      [{uuid, place}] ->
        Logger.debug "Updating place #{uuid}"
        place = %Place{place | description: description, values: config.values}
        :ok = :dets.insert(state.table, {uuid, place})

        # Stop process if needed
        case Map.get(state.by_uuids, uuid) do
          pid when is_pid(pid) -> Process.exit(pid, :kill)
          _ -> nil
        end

        # Restart the place and broadcast update
        new_state = _start_place(state, uuid, place)
        Habbot.PubSub.broadcast(@topic, {:habbot, :place_updated, place})

        {:reply, :ok, new_state}
    end
  end



  # Handle unregister call
  def handle_call({:destroy, place}, _from, state) do
    case Map.get(state.by_uuids, place) do
      pid when is_pid(pid) ->
        Process.exit(pid, :kill)
        :dets.delete(state.table, place)
        Habbot.PubSub.broadcast(@topic, {:habbot, :place_destroyed, %{uuid: place}})

        {:reply, :ok, state}

      _ -> {:reply, :ok, state}
    end
  end


  # Handle list call
  def handle_call(:list, _from, state) do
    res = :dets.foldl(fn {uuid, config}, acc -> Map.put(acc, uuid, config) end, %{}, state.table)
    {:reply, res, state}
  end



  # Handle link call
  def handle_call({:link, place, value}, _from, state) do
    res =
      case Map.get(state.by_uuids, place) do
        pid when is_pid(pid) ->
          config = Habbot.Place.link(pid, value)
          :dets.insert(state.table, {place, config})
          Habbot.PubSub.broadcast(@topic, {:habbot, :place_updated, config})
          :ok

        _ -> :place_not_found
      end

    {:reply, res, state}
  end


  # Handle unlink call
  def handle_call({:unlink, place, value}, _from, state) do
    res =
      case Map.get(state.by_uuids, place) do
        pid when is_pid(pid) ->
          config = Habbot.Place.unlink(pid, value)
          :dets.insert(state.table, {place, config})
          Habbot.PubSub.broadcast(@topic, {:habbot, :place_updated, config})
          :ok

        _ -> :place_not_found
      end

    {:reply, res, state}
  end


  # Handle set_links call
  def handle_call({:set_links, place, values}, _from, state) do
    res =
      case Map.get(state.by_uuids, place) do
        pid when is_pid(pid) ->
          config = Habbot.Place.set_links(pid, values)
          :dets.insert(state.table, {place, config})
          Habbot.PubSub.broadcast(@topic, {:habbot, :place_updated, config})
          :ok

        _ -> :place_not_found
      end

    {:reply, res, state}
  end




  # Handle value process exiting
  def handle_info({:EXIT, from, _reason}, state) do
    # TODO restart the place server ?
    case pop_in(state, [:by_pids, from]) do
      {nil, _pids} -> {:noreply, state}
      {uuid, pids} -> {:noreply, %{state | by_pids: pids, by_uuids: Map.delete(state.by_uuids, uuid)}}
    end
  end


  # Start a place server and update the state accordingly
  def _start_place(state, uuid, config) do
    {:ok, pid} = Place.start_link(config)

    state
    |> put_in([:by_pids, pid], uuid)
    |> put_in([:by_uuids, uuid], pid)
  end

end
