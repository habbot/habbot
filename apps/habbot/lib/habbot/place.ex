defmodule Habbot.Place do

  @moduledoc """
  A place
  """
  use GenServer, start: {__MODULE__, :start_link, [:config]}


  defstruct uuid: "",
    name: "",
    description: nil,
    values: %{}


  @doc """
  Start the server
  """
  def start_link(config) do
    GenServer.start_link(__MODULE__, config)
  end


  @doc """
  Link a value
  """
  def link(pid, value) do
    GenServer.call(pid, {:link, value})
  end


  @doc """
  Unlink a value
  """
  def unlink(pid, value) do
    GenServer.call(pid, {:unlink, value})
  end


  @doc """
  Set the place's links
  """
  def set_links(pid, values) do
    GenServer.call(pid, {:set_links, values})
  end


  # Init the server
  def init(config) do
    Enum.each(config.values, fn {uuid, _} -> Habbot.Values.watch(uuid) end)
    {:ok, config}
  end


  # Handle link call
  def handle_call({:link, value}, _from, state) do
    Habbot.Values.watch(value)
    new_state = %{state | values: Map.put(state.values, value, true)}
    {:reply, new_state, new_state}
  end


  # Handle unlink call
  def handle_call({:unlink, value}, _from, state) do
    Habbot.Values.unwatch(value)
    new_state = %{state | values: Map.delete(state.values, value)}
    {:reply, new_state, new_state}
  end


  # Handle set_links call
  def handle_call({:set_links, values}, _from, state) do
    Enum.each(state.values, fn {uuid, _} -> Habbot.Values.unwatch(uuid) end)
    Enum.each(values, fn {uuid, _} -> Habbot.Values.watch(uuid) end)
    new_state = %{state | values: values}
    {:reply, new_state, new_state}
  end


  # Handle values events
  def handle_info({:habbot, :value_event, _event}, state) do
    {:noreply, state}
  end


  # Ignore other messages
  def handle_info(_, state) do
    {:noreply, state}
  end

end
