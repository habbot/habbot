use Mix.Config

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

config :mysensors,
  measure: :metric,
  networks: [
    %{name: "MQTT Bridge", transport: {MySensors.MQTTBridge, %{}}},
    %{name: "Remote", transport: {MySensors.RemoteBridge, %{node: :"habbot@habbot.home", transport: "023d115c-d157-5b2f-a025-7e9bfabe7d65"}}}
  ]
