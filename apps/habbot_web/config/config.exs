# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config


# Set the main password of the Web application
if File.exists?("root_password.secret.exs"), do: import_config "root_password.secret.exs"


# General application configuration
config :phoenix, :json_library, Jason
config :habbot_web,
  namespace: HabbotWeb


# Configures the endpoint
config :habbot_web, HabbotWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "lhNWPq9ipFgZ5DuoVkZ2uGoxmlrk+k65qP2Od1SYl9oiolpTCizUv+2RVbumoH4U",
  render_errors: [view: HabbotWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: HabbotWeb.PubSubDev,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :habbot_web, :generators,
  context_app: :habbot

config :habbot_web, HabbotWeb.Guardian,
  issuer: "habbot_web",
  secret_key: "secret"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
