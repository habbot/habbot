defmodule HabbotWeb.MySensorsView do
  use HabbotWeb, :view

  alias MySensors.Node
  alias MySensors.Sensor
  alias MySensors.MessageQueue


  # Renders the list of nodes
  def render("networks.json", _params) do
    for {key, value} <- MySensors.networks, into: %{} do
      # TODO maybe use a struct and a JSON encoder implementation instead of this
      {mod, config} = get_in(value, [:config, :transport])

      network =
        value
        |> put_in([:config, :type], _network_type_to_json(mod))
        |> put_in([:config, :transport], config)

      network =
        case network.status do
          :stopped -> network
          {:error, error} ->
            network
            |> Map.put(:status, :error)
            |> Map.put(:error, inspect error)
          {:running, info} ->
            network
            |> Map.put(:status, :running)
            |> Map.put(:info, info)
        end

      {key, network}
    end
  end



  # Renders the list of nodes
  # TODO list the nodes from a disconnected network as offline ?
  def render("nodes.json", _params) do
    MySensors.nodes()
  end


  # Renders the list of sensors
  def render("sensors.json", _params) do
    MySensors.sensors()
  end


  # Render a sensor event
  def render("network_event.json", %{event: event}) do
    alias MySensors.NetworksManager, as: NM

    type =
      case event do
        %NM.NetworkRegistered{} -> :network_registered
        %NM.NetworkUnregistered{} -> :network_unregistered
        %NM.NetworkStatusChanged{} -> :network_status_changed
      end

    %{type: type, data: event}
  end


  # Render a sensor event
  def render("sensor_event.json", %{event: event = %Sensor.ValueUpdatedEvent{}}) do
    event
  end


  # Render a node updated event
  def render("node_command.json", %{event: {status, cmd = %MessageQueue.Item{}}}) do
    cmd
    |> Map.from_struct
    |> Map.put(:status, status)
    |> Map.update!(:id, fn id -> id |> Integer.to_string end)
  end


  # Render the list of variable types
  def render("variable_types.json", _params) do
    for {type, id, desc} <- MySensors.Types.variable, into: %{} do
      {type |> Atom.to_string |> String.replace_prefix("Elixir.", ""), %{id: id, desc: desc}}
    end
  end


  # Render the list of available serial devices on this host
  def render("available_serial_devices.json", _params) do
    MySensors.SerialBridge.enumerate_uarts
  end



  # Render the list of pending commands for the node
  def render("pending_node_commands.json", %{node_uuid: uuid}) do
    commands =
      uuid
      |> MySensors.by_uuid
      |> Node.queue
      |> MessageQueue.list
      |> Enum.map(fn cmd -> render("node_command.json", %{event: {:queued, cmd}}) end)

    %{commands: commands}
  end


  # Render the list of pending commands for the node
  def render("pending_sensor_commands.json", %{sensor_uuid: sensor_uuid}) do
    info =
      sensor_uuid
      |> MySensors.by_uuid
      |> Sensor.info

    sensor_id = Map.fetch!(info, :sensor_id)
    commands =
      info
      |> Map.fetch!(:node)
      |> MySensors.by_uuid
      |> Node.queue
      |> MessageQueue.list
      |> Enum.filter(fn %{message: m} -> match?(%{child_sensor_id: ^sensor_id}, m) end)
      |> Enum.map(fn cmd -> render("node_command.json", %{event: {:queued, cmd}}) end)

    %{commands: commands}
  end




  # Encode atom without the leading 'Elixir.'
  def _encode_atom(atom) do
    atom
    |> Atom.to_string
    |> String.replace_prefix("Elixir.", "")
  end


  def _network_type_to_json(type) do
    case type do
      MySensors.SerialBridge -> "serial"
      MySensors.MQTTBridge -> "mqtt"
      MySensors.RemoteBridge -> "remote"
    end
  end


  def _network_type_from_json(type) do
    case type do
      "serial" -> MySensors.SerialBridge
      "mqtt" -> MySensors.MQTTBridge
      "remote" -> MySensors.RemoteBridge
    end
  end

end


# JSon encoder for Message struct
defimpl Jason.Encoder, for: MySensors.Message do
  import HabbotWeb.MySensorsView, only: [_encode_atom: 1]
  def encode(msg, options) do
    msg
    |> Map.from_struct
    |> Map.update!(:type, fn t -> _encode_atom(t) end)
    |> Jason.Encoder.encode(options)
  end
end

#
# JSon encoder for Network struct
defimpl Jason.Encoder, for: MySensors.Network do
  import HabbotWeb.MySensorsView, only: [_encode_atom: 1]
  def encode(node, options) do
    node
    |> Map.from_struct
    |> Map.take([:id, :uuid])
    |> Jason.Encoder.encode(options)
  end
end


# JSon encoder for Node struct
defimpl Jason.Encoder, for: MySensors.Node do
  import HabbotWeb.MySensorsView, only: [_encode_atom: 1]
  def encode(node, options) do
    node
    |> Map.from_struct
    |> Map.drop([:pid, :sensors])
    |> Map.put(:sensors_count, (if Map.has_key?(node, :sensors), do: Map.size(node.sensors), else: 0))
    |> Map.update!(:type, fn t -> _encode_atom(t) end)
    |> Jason.Encoder.encode(options)
  end
end


# JSon encoder for Sensor struct
defimpl Jason.Encoder, for: MySensors.Sensor do
  import HabbotWeb.MySensorsView, only: [_encode_atom: 1]
  def encode(sensor, options) do
    sensor
    |> Map.from_struct
    |> Map.update!(:type, fn t -> _encode_atom(t) end)
    |> Map.update!(:data, fn data ->
      for {type, {value, time}} <- data, into: %{}, do: {_encode_atom(type), %{value: value, time: time}}
    end)
    |> Jason.Encoder.encode(options)
  end
end


# JSon encoder for ValueUpdatedEvent struct
defimpl Jason.Encoder, for: MySensors.Sensor.ValueUpdatedEvent do
  import HabbotWeb.MySensorsView, only: [_encode_atom: 1]
  def encode(event, options) do
    {new_value, new_time} = event.new
    {old_value, old_time} = case event.old do
      nil -> {nil, nil}
      other -> other
    end

    event
    |> Map.from_struct
    |> Map.update!(:type, fn t -> _encode_atom(t) end)
    |> Map.put(:new, %{value: new_value, time: new_time})
    |> Map.put(:old, %{value: old_value, time: old_time})
    |> Jason.Encoder.encode(options)
  end
end


# JSon encoder for NetworkRegistered struct
defimpl Jason.Encoder, for: MySensors.NetworksManager.NetworkRegistered do
  import HabbotWeb.MySensorsView, only: [_network_type_to_json: 1]
  def encode(event, options) do
    {bridge, transport_config} = Map.get(event.config, :transport)

    # TODO use a struct as in networks.json list
    %{
      uuid: event.network,
      status: :stopped,
      config: %{
        name: event.config.name,
        type: _network_type_to_json(bridge),
        transport: transport_config,
      }
    }
    |> Jason.Encoder.encode(options)
  end
end

# JSon encoder for NetworkUnregistered struct
defimpl Jason.Encoder, for: MySensors.NetworksManager.NetworkUnregistered do
  def encode(event, options) do
    event
    |> Map.from_struct
    |> Jason.Encoder.encode(options)
  end
end


# JSon encoder for NetworkStatusChanged struct
defimpl Jason.Encoder, for: MySensors.NetworksManager.NetworkStatusChanged do
  def encode(event, options) do
    event = Map.from_struct(event)

    case event.status do
      :stopped -> event
      {:running, info} ->
        event
        |> Map.put(:status, :running)
        |> Map.put(:info, info)
      {:error, error} ->
        event
        |> Map.put(:status, :error)
        |> Map.put(:error, inspect error)
    end
    |> Jason.Encoder.encode(options)
  end
end


