defmodule HabbotWeb.MySensorsNetworkChannel do
  use Phoenix.Channel
  require Logger


  # OnJoin: subscribe to network events
  def join("mysensors#network:" <> uuid, _params, socket) do
    transport_uuid =
      uuid
      |> MySensors.by_uuid
      |> MySensors.Network.info
      |> Map.fetch!(:transport_uuid)

    # Subscribe to transport events
    MySensors.TransportBus.subscribe_incoming(transport_uuid)
    MySensors.TransportBus.subscribe_outgoing(transport_uuid)

    {:ok, assign(socket, :network_uuid, uuid)}
  end


  # Message: Send a command to the network
  def handle_in("command", message, socket) do
    Map.get(socket.assigns, :network_uuid)
    |> MySensors.by_uuid
    |> MySensors.Network.send_message(MySensors.Message.new(
      message["node_id"],
      message["sensor_id"],
      MySensors.Types.command_type(message["command"]),
      MySensors.Types.variable_type(message["type"]),
      message["payload"]
    ))

    {:reply, {:ok, %{status: :sent}}, socket}
  end


  # Message: Fallback for unrecognized messages
  def handle_in(msg, payload, socket) do
    Logger.warn "#{__MODULE__} received an unexpected message: #{msg}\n#{inspect payload}"
    {:noreply, socket}
  end


  # Event: forward incoming messages through the channel
  def handle_info({:mysensors, :incoming, message}, socket) do
    push(socket, "incoming", message)
    {:noreply, socket}
  end


  # Event: forward outgoing messages through the channel
  def handle_info({:mysensors, :outgoing, message}, socket) do
    push(socket, "outgoing", message)
    {:noreply, socket}
  end


end
