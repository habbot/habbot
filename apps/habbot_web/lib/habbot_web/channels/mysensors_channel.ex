defmodule HabbotWeb.MySensorsChannel do
  use Phoenix.Channel
  require Logger

  alias HabbotWeb.MySensorsView


  # OnJoin: subscribe to events
  def join("mysensors:" <> _subtopic, _params, socket) do
    MySensors.NetworksManager.subscribe_networks_events
    MySensors.Node.subscribe_nodes_events
    MySensors.Sensor.subscribe_sensors_events

    {:ok, socket}
  end


  # Message: list the networks
  def handle_in("networks", _payload, socket) do
    {:reply, {:ok, MySensorsView.render("networks.json")}, socket}
  end


  # Message: list the nodes
  def handle_in("nodes", _payload, socket) do
    {:reply, {:ok, MySensorsView.render("nodes.json")}, socket}
  end


  # Message: list the sensors
  def handle_in("sensors", _payload, socket) do
    {:reply, {:ok, MySensorsView.render("sensors.json")}, socket}
  end


  # Message: start a network scan
  def handle_in("scan_network", %{"uuid" => uuid}, socket) do
    uuid
    |> MySensors.by_uuid
    |> MySensors.Network.scan

    {:reply, :ok, socket}
  end


  # Message: start a network
  def handle_in("start_network", %{"uuid" => uuid}, socket) do
    case MySensors.start_network(uuid) do
      {:ok, _pid} -> {:reply, {:ok, %{status: :ok}}, socket}
      {:error, reason} -> {:reply, {:ok, %{status: :error, error: inspect reason}}, socket}
    end
  end


  # Message: stop a network
  def handle_in("stop_network", %{"uuid" => uuid}, socket) do
    MySensors.stop_network(uuid)
    {:reply, :ok, socket}
  end


  # Message: list the variable types
  def handle_in("variable_types", _payload, socket) do
    {:reply, {:ok, MySensorsView.render("variable_types.json")}, socket}
  end


  # Message: list the serial devices
  def handle_in("available_serial_devices", _payload, socket) do
    {:reply, {:ok, MySensorsView.render("available_serial_devices.json")}, socket}
  end

  # Message: register a new network
  def handle_in("register_network", %{"name" => name, "type" => type, "transport" => transport}, socket) do
    uuid = MySensors.NetworksManager.register_network(%{
      name: name,
      transport: {MySensorsView._network_type_from_json(type), transport}
    })

    {:reply, {:ok, %{uuid: uuid}}, socket}
  end


  # Message: unregister a network
  def handle_in("unregister_network", %{"uuid" => uuid}, socket) do
    MySensors.NetworksManager.unregister_network(uuid)
    {:reply, :ok, socket}
  end

  # Message: update a network
  def handle_in("update_network", _config, socket) do
    {:reply, {:error, %{reason: :not_yet_implemented}}, socket}
  end



  # Message: fallback for unrecognized messages
  def handle_in(msg, payload, socket) do
    Logger.warn "#{__MODULE__} received an unexpected message: #{msg}\n#{inspect payload}"
    {:noreply, socket}
  end


  # Event: forward incoming messages
  def handle_info({:mysensors, :incoming, message}, socket) do
    push(socket, "incoming", message)
    {:noreply, socket}
  end


  # Event: forward outgoing messages
  def handle_info({:mysensors, :outgoing, message}, socket) do
    push(socket, "outgoing", message)
    {:noreply, socket}
  end


  # Event: forward sensor events
  def handle_info({:mysensors, :sensors_events, event}, socket) do
    push(socket, "sensors_events", MySensorsView.render("sensor_event.json", event: event))
    {:noreply, socket}
  end


  # Event: forward node events
  def handle_info({:mysensors, :nodes_events, event}, socket) do
    alias MySensors.Node

    case event do
      %Node.NodeDiscoveredEvent{specs: specs} -> push(socket, "node_discovered", specs)
      %Node.NodeUpdatedEvent{uuid: uuid, info: info} -> push(socket, "node_updated", Map.put(info, :uuid, uuid))
    end

    {:noreply, socket}
  end


  # Event: forward network events
  def handle_info({:mysensors, :networks_events, event}, socket) do
    push(socket, "networks_events", MySensorsView.render("network_event.json", event: event))
    {:noreply, socket}
  end


  # Event: forward message queue events
  def handle_info({:mysensors, :node_commands, {:cleared}}, socket) do
    push(socket, "node_commands_cleared", %{})
    {:noreply, socket}
  end

end
