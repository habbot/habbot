defmodule HabbotWeb.HabbotChannel do
  use Phoenix.Channel
  require Logger

  alias HabbotWeb.HabbotView


  def join("habbot:" <> _subtopic, _params, socket) do
    Habbot.Values.watch
    Habbot.Places.watch
    {:ok, socket}
  end


  def handle_in("time", _payload, socket) do
    {:reply, {:ok, HabbotView.render("time.json")}, socket}
  end


  def handle_in("values", _payload, socket) do
    {:reply, {:ok, HabbotView.render("values.json")}, socket}
  end


  def handle_in("places", _payload, socket) do
    {:reply, {:ok, HabbotView.render("places.json")}, socket}
  end


  def handle_in("create_place", %{"name" => name, "description" => description, "config" => config}, socket) do
    case Habbot.Places.create(name, description, %{values: Map.get(config, "values")}) do
      {:ok, uuid} -> {:reply, {:ok, %{uuid: uuid}}, socket}
      _ -> {:reply, :error, socket}
    end
  end

  def handle_in("update_place", %{"uuid" => uuid, "description" => description, "config" => config}, socket) do
    case Habbot.Places.update(uuid, description, %{values: Map.get(config, "values")}) do
      :ok -> {:reply, {:ok, %{uuid: uuid}}, socket}
      _ -> {:reply, :error, socket}
    end
  end



  def handle_in("delete_place", %{"uuid" => uuid}, socket) do
    Habbot.Places.destroy(uuid)
    {:reply, :ok, socket}
  end


  def handle_in("update_place_values", %{"uuid" => uuid, "values" => values}, socket) do
    Habbot.Places.set_links(uuid, values)
    {:reply, :ok, socket}
  end


  # TODO Update this to use the same match used in the generic definition below this one
  def handle_info({:habbot, :value_event, event = %{}}, socket) do
    push(socket, "value_updated", HabbotView.render("value_event.json", type: "value_updated", payload: event))
    {:noreply, socket}
  end
  def handle_info({:habbot, type, payload}, socket) when type in [:value_updated, :value_outdated, :value_uptodate] do
    push(socket, Atom.to_string(type), HabbotView.render("value_event.json", type: type, payload: payload))
    {:noreply, socket}
  end


  def handle_info({:habbot, type, payload}, socket) when type in [:place_created, :place_updated, :place_destroyed] do
    push(socket, Atom.to_string(type), HabbotView.render("place_event.json", type: type, payload: payload))
    {:noreply, socket}
  end

end

