defmodule HabbotWeb.PageController do
  use HabbotWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
