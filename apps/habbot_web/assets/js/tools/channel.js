import socket from './socket'


function isRunning(command) {
  return command ? command.running == true : false
}


function initCommands(config) {
  let commands = {}
  let base = { running: false }
  if(config) config.forEach(cmd => { commands[cmd.id] = Object.assign({}, base, cmd) })
  return commands
}


function registerHandlers(vm, channel, config) {
  if(!Array.isArray(config))
    return []

  return config.map(handler => {
    let ref = channel.on(handler.event, (msg) => handler.fun(vm, msg))
    return {ref, ...handler}
  })
}


export function usingChannel(config) {
  return {

    data: function() { return {
      _channels: {
        channel: config.channel ? config.channel(this) : null,
        commands: initCommands(config.commands),
        handlers: [],
      }
    }},

    computed: {
      _channel: function() { return this._data._channels.channel },
      anyCommandRunning: function() { return Object.values(this._data._channels.commands).some(isRunning) },
      isCommandRunning: function() { return command => isRunning(this._data._channels.commands[command]) }
    },

    watch: {
      _channel: function(v, ov) {
        if(v == null && ov != null) {
          this.cleanChannel(ov)
          this.onChannelDisconnected()
        } else if(v != null && v != ov) {
          this.cleanChannel(ov)
          this.onChannelDisconnected()

          this.setupChannel(v)
          this.onChannelConnected(v)
        }
      }
    },

    beforeDestroy: function() {
      this.cleanChannel(this._channel)
      this.onChannelDisconnected()
    },

    methods: {
      onChannelConnected: function(channel) {},
      onChannelDisconnected: function() {},

      // Setup handlers
      setupChannel: function(channel) {
        if(channel == null) return
        this._data._channels.handlers = registerHandlers(this, channel, config.handlers)
      },

      // Clean handlers
      cleanChannel: function(channel) {
        if(channel == null) return
        this._data._channels.handlers.forEach(handler => channel.off(handler.event, handler.ref))
      },

      // Send a command
      sendCommand: function(command, parameters, ok, error, timeout) {
        let cmd = this._data._channels.commands[command]
        if(cmd == null) {
          console.error(`Unknown command ${command}`)
          return null
        }

        cmd.running = true
        this._channel.push(cmd.name || cmd.id, parameters)
          .receive("ok", resp => {
            cmd.running = false
            this.processHandler(ok || cmd.ok, resp)
          })
          .receive("error", resp => {
            cmd.running = false
            this.processHandler(error || cmd.error || this.$t('command.error'), resp)
          })
          .receive("timeout", () => {
            cmd.running = false
            this.processHandler(timeout || cmd.timeout || this.$t('command.timeout'))
          })
      },

      // Process a command handler
      processHandler: function(handler, resp) {
        if(typeof(handler) == 'function')
          handler(this, resp)
        else if(typeof(handler) == 'string')
          this.showSnackbar(this.$t(handler, {response: resp}))
      },

    }
  }
}


export function withChannel(config) {
  return {
    mixins: [ usingChannel(config) ],

    computed: {
      channel: function() { return this._channel },
      channelName: function() {
        if(config == null) return null

        let name = config.name
        let type = typeof name

        if(type == "string")
          return name
        else if(type == "function")
          return name.call(this, this)
        else
          return null
      }
    },

    mounted: function() {
      if(this.channelName == null) return

      let channel = socket.channel(this.channelName, {})
      channel.join()
        .receive("ok", (res) => {
          this._data._channels.channel = channel
          if(config.onJoin) config.onJoin(this, res)
        })
        //TODO handle errors
    },

    beforeDestroy: function() {
      if(this.channel != null)
        this.channel.leave()

      this._data._channels.channel = null
    },

  }
}
