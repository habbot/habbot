import "phoenix_html"


// Vue
import Vue from 'vue'

/*
 * Vuetify
 */
import Vuetify from 'vuetify/lib'
import {
  VList,
  VListTile,
  VListTileContent,
  VListTileTitle,
  VListTileAction,
  VTooltip,
  VContainer,
  VLayout,
  VFlex,
  VSpacer,
  VCard,
  VCardTitle,
  VCardText,
  VCardActions,
  VBtn,
  VIcon,

  VApp,
  VToolbar,
  VToolbarSideIcon,
  VToolbarTitle,
  VContent,
  VFooter,
  VBottomNav,

  VDialog,
  
} from 'vuetify/lib'

Vue.use(Vuetify, {
  theme: {
    primary: "#4CAF50", // green 500
    accent: "#795548", // brown 500
  },
  components: {
    VList,
    VListTile,
    VListTileContent,
    VListTileTitle,
    VListTileAction,
    VTooltip,
    VContainer,
    VLayout,
    VFlex,
    VSpacer,
    VCard,
    VCardTitle,
    VCardText,
    VCardActions,
    VBtn,
    VIcon,

    VApp,
    VToolbar,
    VToolbarSideIcon,
    VToolbarTitle,
    VContent,
    VFooter,
    VBottomNav,

    VDialog,
  }
})


/*
 * Other modules
 */
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueWait from 'vue-wait'
import VueI18n from 'vue-i18n'
import Vuelidate from 'vuelidate'

import '../css/app.styl'
import '../css/app.scss'


Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueWait)
Vue.use(VueI18n)
Vue.use(Vuelidate)

// Workaround for https://github.com/vuematerial/vue-material/issues/1977
// cannot read property $options of undefined
Vue.component('router-link', Vue.options.components.RouterLink);
Vue.component('router-view', Vue.options.components.RouterView);



// Application
require('tools/helpers').default.init()
import App from './app'
import router from './router'
import store from './store'
import translations from './translations'

let currentLocale = window.navigator.languages.find(e => translations[e] != null)

new Vue({
  el: '#root',
  router,
  store,
  wait: new VueWait({useVuex: true}),
  i18n: new VueI18n({
    locale: currentLocale,
    fallbackLocale: 'en',
    messages: translations,
    silentTranslationWarn: true
  }),
  components: { App }
});
