import VueRouter from 'vue-router'

import MainUI from './main.vue'

import Home from './home'
import Places from './places/places'
import PlaceDialog from './places/place_dialog'
import EditPlaceDialog from './places/edit_place_dialog'
import Values from './values/values'
import Schedules from './schedules'
import Alerts from './alerts'
import Settings from './settings'

import MySensors from './mysensors/router'


// Base routes
const router = new VueRouter({
  routes: [
    /*
     * Habbot routes
     */
    { path: '/place/new', component: EditPlaceDialog, props: (route) => ({creation: true}) },
    { path: '/place/:place/edit', component: EditPlaceDialog, props: (route) => ({place: route.params.place}) },
    { path: '/place/:place', component: PlaceDialog, props: (route) => ({place: route.params.place}) },

    { path: '/', component: MainUI,
      children: [
        { path: '/', redirect: 'home' },
        { path: 'home', component: Home },
        { path: 'places', component: Places },
        { path: 'values', component: Values },
        { path: 'schedules', component: Schedules },
        { path: 'alerts', component: Alerts },
        { path: 'settings', component: Settings },
      ]
    },


    /*
     * Modules
     */
    MySensors
  ]
})


export default router
